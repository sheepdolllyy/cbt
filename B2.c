#include <suitesparse/umfpack.h>
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "math.h"

int main () {
    int num;
    FILE *f;
    do {
        printf("Press [1], [2], [3], [4] to choose file \n 1.(1.dat) \n \
2.(2.dat) \n 3.(3.dat) \n 4.(4.dat) \n");
        printf("Waiting for input: ");
        scanf("%d", &num);
        if( num == 1 )
            f = fopen("1.dat", "r");
        else if( num == 2 )
            f = fopen("2.dat", "r");
        else if( num == 3 )
            f = fopen("3.dat", "r");
        else if( num == 4 )
            f = fopen("4.dat", "r");
        else 
            printf("Wrong number!!!\n");
    } while( num != 1 && num != 2 && num != 3 && num != 4 );
	int N, N0, hlp;
    int *Astb = NULL, *Ai = NULL;
    double *A = NULL;
    void *buff = NULL;
    clock_t start1, start2;
    
    fscanf(f, "%d", &N);
    printf("Size %d\n", N);
    
    //считыаем индексы столбцов ненулевых элементов
	for (int i = 0; i < N + 1; i++) {
		buff = realloc(Astb, sizeof(int*) * (i + 1));
		Astb = buff;
		fscanf(f, "%d", &hlp);
		Astb[i] = hlp - 1;
	}
	N0 = Astb[N] + 1;
	
    //считываем указатели на начало каждой строки
    for (int i = 0; i < N0; i++) {
		buff = realloc(Ai, sizeof(int*) * (i + 1));
		Ai = buff;
		fscanf(f, "%d", &hlp);
		Ai[i] = hlp - 1;
	}
	
	//считываем матрицу
	for (int i = 0; i < N0; i++) {
		buff = realloc(A, sizeof(double*) * (i + 1));
		A = buff;
		fscanf(f, "%lf", &A[i]);
	}

	double *b = malloc(sizeof(double) * N);
    double *x = malloc(sizeof(double*) * N);
    double *null = ( double * ) NULL;
    void *symbolic, *numeric;
    
    for (int i = 0; i < N; i++) 
        b[i] = sin(i);          

	umfpack_di_symbolic(N, N, Astb, Ai, A, &symbolic, null, null); 
    umfpack_di_numeric(Astb, Ai, A, symbolic, &numeric, null, null);

	printf("Factorization time: %.4lf\n", (double)(clock() - start1) 
/ (double)(CLOCKS_PER_SEC));

    umfpack_di_free_symbolic(&symbolic);
    
    start2 = clock();

	umfpack_di_solve(UMFPACK_At, Astb, Ai, A, x, b, numeric, null, null);

	printf("Solving time: %.4lf\n", (double)(clock() - start2) / 
(double)(CLOCKS_PER_SEC));

	umfpack_di_free_numeric(&numeric);

	free(Ai);
	free(Astb);
	free(A);
	free(b);
	free(x);
	fclose(f);
	return 0;
}